---
date: "2019-09-29"
title: "Principal"
---

Soy **Galaxy** y tengo muchas ganas de enseñarte mi blog.

Este blog está hecho para ti, para que disfutes y te relajes.

Aquí verás entradas de:

* Información
* Aplicaciones
* Manualidades
* ... y muchas cosas más


Usa Software Libre y si quieres, contribuye en [GitHub](https://github.com/gohugoio).
