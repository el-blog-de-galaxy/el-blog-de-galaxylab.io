---
title: "La contaminación en la Tierra"
date: 2019-10-05
---
# **Contaminación en el mar**

![Comecocos](/tortuga.jpg)


La contaminación esta afectando a nuestro alrededor, en este caso el mar. Los oceanos se ensucian. Los animales marinos estan bien. Algunas especies se extinguen. Para ayudar solo tienes que colaborar con [Greenpeace](https://es.greenpeace.org/es/que-puedes-hacer-tu/peticiones/)


# **Contaminación en el aire**

![Comecocos](/contaminacion-del-aire1-e1538514477707.jpg)

El aire es muy importante para la vida de todos los seres vivos que lo respiran. Si no se cuida, todos esos seres se extinguirian con el paso del tiempo. Nosotras las personas, no lo estamos cuidando. Para ayudar solo tienes que colaborar con [Greenpeace](https://es.greenpeace.org/es/que-puedes-hacer-tu/peticiones/)
