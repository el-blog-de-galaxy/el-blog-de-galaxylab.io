---
title: "Manualidades"
date: 2019-10-05
---

# **Comecocos de papel** 

![Comecocos](/comecocos.jpg)

El comecocos, una manualidad muy facil, y que podrás disfrutar con tus amigos

Si quieres aprender a hacer uno haz clic [aquí](https://www.youtube.com/watch?v=di5I5OubriU).

# **Avión de papel**
![Avión](/avión de papel.jpg)

El avión de papel, lo más sencillo del mundo de origami.

Si quieres aprender a hacer uno haz clic [aquí](https://www.youtube.com/watch?v=44iMaTOd89A)
